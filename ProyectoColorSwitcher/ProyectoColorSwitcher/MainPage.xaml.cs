﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace ProyectoColorSwitcher
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
        }

        private void ChangeColor(object sender, EventArgs e)
        {
            var Red = RedSlider.Value;
            var Green = GreenSlider.Value;
            var Blue = BlueSlider.Value;
            var Alpha = AlphaSlider.Value;
            Color color = new Color(Red,Green,Blue,Alpha);

            BoxColor.BackgroundColor = color;
            LblDisplay.Text = ShowHexadecimal(color);
        }
        private string ShowHexadecimal(Color color)
        {
            int rojo = Convert.ToInt32(color.R * 255);
            int verde = Convert.ToInt32(color.G * 255);
            int azul = Convert.ToInt32(color.B * 255);
            int opacidad = Convert.ToInt32(color.A);
            
            return $"#{rojo:X2}{verde:X2}{azul:X2}{opacidad:X2}";
        }


    }
}
